require 'date'
require_relative 'car'
require_relative 'rental'

module Util
  extend self

  def parse_cars(data)
    cars = {}
    data['cars'].each do |car_hash|
      car = Car.new(car_hash)
      cars[car.id] = car
    end

    cars
  end

  def parse_rentals(data)
    cars = parse_cars(data)
    rentals = {}
    data['rentals'].each do |rental_hash|
      car = cars[rental_hash['car_id']]
      rental = Rental.new(rental_hash, car)
      rentals[rental.id] = rental
    end

    rentals
  end

  def modify_rental(rental, modification)
    rental = rental.dup
    modification = modification
                     .inject({}){ |hash,(k,v)| hash[k.to_sym] = v; hash }
                     .select { |k,_v| !%i(id rental_id).include?(k) }
    modification.each do |key, value|
      method_name = "#{key}="
      value = (key == :start_date || key == :end_date) ? Date.parse(value) : value
      rental.send(method_name, value) if rental.respond_to?(method_name)
    end
    rental
  end

end
