class Rental
  attr_accessor :id
  attr_accessor :car
  attr_accessor :start_date
  attr_accessor :end_date
  attr_accessor :distance
  attr_accessor :options

  DISCOUNTS = [
    { from_day: 2, to_day: 4, discount: 0.1 },
    { from_day: 5, to_day: 10, discount: 0.3 },
    { from_day: 11, to_day: nil, discount: 0.5 },
  ]

  class << self
    def delta_actions(old_rental, new_rental)
      delta = []
      old_actions = old_rental.actions
      new_actions = new_rental.actions
      new_actions.each do |new_action|
        old_action = old_actions.find { |a| a[:who] == new_action[:who] }
        amount = new_action[:amount] - old_action[:amount]
        type = amount < 0 ? flip_type(old_action[:type]) : old_action[:type]

        delta.push({
                     who: new_action[:who],
                     type: type,
                     amount: amount.abs
                   })
      end

      delta
    end

    private

    def flip_type(old_type)
      case old_type
      when :debit
        :credit
      else
        :debit
      end
    end
  end

  def initialize(rental, car)
    @id = rental['id']
    @car = car
    @start_date = Date.parse(rental['start_date'])
    @end_date = Date.parse(rental['end_date'])
    @distance = rental['distance']
    @options = {
      deductible_reduction: rental['deductible_reduction']
    }

  rescue ArgumentError => ex
    puts "Invalid date found in rental ID #{rental['id']}: #{ex}"
    throw ex
  end

  def days
    @end_date - @start_date + 1
  end

  def deductible_reduction=(val)
    @options[:deductible_reduction] = val
  end

  def price
    price_by_days = calculate_price_by_days
    price_by_km = (distance * car.price_per_km).to_i

    price_by_days + price_by_km
  end

  def commission
    total_commission = price * 0.3
    insurance = 0.5 * total_commission
    assistance = 100 * days
    {
      insurance_fee: insurance.to_i,
      assistance_fee: assistance.to_i,
      drivy_fee: (total_commission - insurance - assistance).to_i
    }
  end

  def options_costs
    costs = {}

    costs[:deductible_reduction] = if options[:deductible_reduction]
                                       (400 * days).to_i
                                     else
                                       0
                                     end

    costs
  end

  def actions
    total_option_costs = options_costs.values.inject(0, :+).to_i

    [
      {
        who: 'driver',
        type: :debit,
        amount: price + total_option_costs
      },
      {
        who: 'owner',
        type: :credit,
        amount: (price - commission.values.inject(0, :+)).to_i
      },
      {
        who: 'insurance',
        type: :credit,
        amount: commission[:insurance_fee]
      },
      {
        who: 'assistance',
        type: :credit,
        amount: commission[:assistance_fee]
      },
      {
        who: 'drivy',
        type: :credit,
        amount: commission[:drivy_fee] + total_option_costs
      }
    ]
  end

  private

  def calculate_price_by_days

    price = days * car.price_per_day

    discounts = DISCOUNTS.map do |discount|
      if days < discount[:from_day]
        0
      else
        if discount[:to_day]
          period = discount[:to_day] - discount[:from_day] + 1
          period = [period, days - discount[:from_day] + 1].min
        else
          period = days - discount[:from_day] + 1
        end
        period * (car.price_per_day * discount[:discount])
      end
    end

    (price - discounts.inject(0, :+)).to_i
  end
end
