#!/usr/bin/ruby -w

require 'json'
require_relative 'util'
require_relative 'rental'

def init(level)
  file = File.join(File.dirname(__FILE__), "../#{level}/data.json")
  raise StandardError, "Cannot find `data.json` for #{level}" unless File.file?(file)

  data = JSON.parse(File.read(file))

  rentals = Util.parse_rentals(data)

  output = { rental_modifications: [] }

  data['rental_modifications'].each do |modification|
    old_rental = rentals[modification['rental_id']]
    new_rental = Util.modify_rental(old_rental, modification)

    delta_actions = Rental.delta_actions(old_rental, new_rental)

    output[:rental_modifications].push({
      id: modification['id'],
      rental_id: modification['rental_id'],
      actions: delta_actions
    })
  end

  test(level, output)

rescue => ex
    puts "Catastrophic error: #{ex.message}"
    puts ex.backtrace
    exit(1)
end

def test(level, data)
  file = File.join(File.dirname(__FILE__), "../#{level}/output.json")
  raise StandardError, "Cannot find `output.json` for #{level}" unless File.file?(file)

  output = JSON.parse(File.read(file))

  if output.to_json == data.to_json
    puts 'Congrats, the output is the same!'
  else
    puts 'There is an issue with your output'
    puts 'Your generated output:'
    puts data.to_json
    puts 'The expected output:'
    puts output.to_json
  end
rescue => ex
  puts "Catastrophic error when testing result: #{ex.message}"
  exit(1)
end

def main
  if ARGV[0]
    init(ARGV[0])
  else
    puts 'Please pass the level to solve. Example: ./drivy level1'
  end
end

main
