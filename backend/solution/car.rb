class Car
  attr_accessor :id
  attr_accessor :price_per_day
  attr_accessor :price_per_km

  def initialize(hash)
    @id = hash['id']
    @price_per_day = hash['price_per_day']
    @price_per_km = hash['price_per_km']
  end
end
